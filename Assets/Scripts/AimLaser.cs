﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
public class AimLaser : MonoBehaviour
{
    [SerializeField]float EmptyAimLenth;
    [SerializeField]float SingleLen;
    [SerializeField]Transform EndGO;
    RaycastHit ray;
    GameManager GM;
    float AimStartPoint;
    bool aimHited;
    MovementPath path;
    bool active;

    void Start()
    {
     GM = GetComponentInParent<GameManager>();
     aimHited = false;
     AimStartPoint =transform.position.z;
    //  SingleLen*=GameScaler.factor;
    active = true;
    }

    void Update()
    {   
            // if(MovementPath.final||GM.LVLFailed){
            //     if(active){
            //         active = false;
            //         for (int i = 0; i < transform.childCount; i++)
            //         {
            //             transform.GetChild(i).gameObject.SetActive(false);
            //         }
            //         EndGO.gameObject.SetActive(false);
            //     }
            //     return;
            // }
            if(!active&&!GameManager.GameStopped){
                    active = true;
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        transform.GetChild(i).gameObject.SetActive(true);
                    }
                    EndGO.gameObject.SetActive(true);
                }
        RaycastHit[] Rays = Physics.RaycastAll(transform.position,Vector3.forward,30);
                  aimHited = false; 
            RaycastHit minRay = new RaycastHit() ;
            bool hasMinRay = false;
            foreach (var ray in Rays)
            {
                    if (ray.collider != null&&(ray.collider.tag == "ActiveBalls"))
                {   
                    
                    if(!hasMinRay||ray.point.z<minRay.point.z) {
                        minRay=ray;
                        hasMinRay=true;
                    }
                   
                                
                }   

            }
         if (hasMinRay){
               float scale = (float)(minRay.distance)/SingleLen/GameScaler.factor;
                transform.localScale = new Vector3(1,scale,1);
                EndGO.localPosition = new Vector3(0,1,minRay.distance/GameScaler.factor);
                aimHited = true;
            }

        if(!aimHited){
            // Debug.Log(EmptyAimLenth);
            // Debug.Log(SingleLen);
            // Debug.Log(GameScaler.factor);
             transform.localScale = new Vector3(1,EmptyAimLenth,1);
             EndGO.localPosition = new Vector3(0,1,EmptyAimLenth*SingleLen);
           
        }

        

 
    }

    public void HideAim(){

    }
    

}
