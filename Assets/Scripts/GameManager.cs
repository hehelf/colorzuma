﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BansheeGz.BGSpline.Components;
using UnityEngine.UI;
using DG.Tweening;
public class GameManager : MonoBehaviour
{
    public float BallsSpeed;
    public float BallFlightSpeed;
    public float FlightLength;
    public int mapNum;
    [SerializeField]int totalMaps;
    public GameObject BallPrefab;
    public bool UpdColorOn;
    public static bool GameStopped = false;
    public GameObject PathPrefab;
     [SerializeField]GameObject finScreen;
      [SerializeField]GameObject settingsGo;
    // [SerializeField]Animation ComboAnim;
    [SerializeField]Text StartDelayText;
     [SerializeField]GameObject PlayButton;
     [SerializeField]GameObject Puf;
    MovementPath path;
    GameObject pathGO1;

         private int prevChildrenCount=0;
         BallSpawner spawner;
    Coroutine StartDelayCoroutine;
    bool islvlsaved;
    ScoreManager SM;
     Cannon cannon;
    public ImageConrtol IMGC;
    public bool LVLFailed =false;
    void Start()
    {
        GameStopped = false;
       cannon = GetComponentInChildren<Cannon>();
       
    }

    // Update is called once per frame
    void Update()
    {
        if(pathGO1==null){
            LVLFailed = false;
            GameStopped = false;
            // SM.UpdText();
            SpawnPath();
            islvlsaved=false;
            cannon.UpdBallsInCannon();
            return;
        }


     if(GameManager.GameStopped) return;
     
     FillCheck();
    // if( prevChildrenCount ==0) prevChildrenCount =path.transform.childCount;
    //    if(path.transform.childCount==0&&prevChildrenCount>0&&spawner.CurrenWave==spawner.Waves.Count-1){
    //             final(true);
    //         }

    }

    void FillCheck(){
        int i =0;
                foreach (var part in IMGC.PartsFill)
        {
            if(part>=1) i++;
        }

            if(i==IMGC.PartsFill.Length) final(true);
    }
   void SpawnPath(){
        if(mapNum<1||mapNum>totalMaps){mapNum = 1;}
        PathPrefab = Resources.Load("maps/"+mapNum) as GameObject;
            pathGO1 = Instantiate(PathPrefab,Vector3.zero,Quaternion.identity,transform);
              path =  GetComponentInChildren<MovementPath>();
            spawner = GetComponentInChildren<BallSpawner>();
            IMGC = GetComponentInChildren<ImageConrtol>();
            
   }
       public void Play(){
        PlayButton.SetActive(false);
        StartDelayCoroutine = StartCoroutine(StartDelay());  
    }
        public void final(bool win){
        DOTween.KillAll();
        GameStopped = true;
        finScreen.SetActive(true);
        Text txt = finScreen.GetComponentInChildren<Text>();
        mapNum++;// development))
        if(win){
            txt.text= "Win";
            txt.color = Color.green;
            // mapNum++;
        }else{
            txt.text= "wasted";
            txt.color = Color.red;
        }
        
        }
    
        public void pause(){
        // if(settingsGo.activeSelf)return;
         GameStopped = true;
        PlayButton.SetActive(true);
        if(StartDelayText.gameObject.activeSelf)StartDelayText.gameObject.SetActive(false);
            if (StartDelayCoroutine!=null)StopCoroutine(StartDelayCoroutine);
        // StopAllCoroutines();
        // StopCoroutine()
    }
        void OnApplicationFocus(bool hasFocus)
    {   if(hasFocus) return;
        if((!finScreen.activeSelf&&!MovementPath.final)){
            // PlayerPrefs.SetInt("inProgress",1);
            // SaveLevelProgress();
            
          pause();
        
        }else{
            // if(MovementPath.final){
            //     TryIncAndSave();
            // }
            // PlayerPrefs.SetInt("inProgress",0);
            
            //  Debug.Log("ne save  ");
        }
        // SM.SaveScore();
        // Debug.Log("Application focus  " + hasFocus);
    }
    public void restart(){
        prevChildrenCount=0;
        GameStopped =true;
        finScreen.SetActive(false);
        cannon.UpdBallsInCannon();
        Destroy(pathGO1);
                        // SceneManager.LoadScene("Game");
    }

            IEnumerator StartDelay(){
            // float sec = 3f;
            StartDelayText.gameObject.SetActive(true);
            // StartDelayText.text="3";
            // yield return new WaitForSeconds(sec);
            // StartDelayText.text="2";
            // yield return new WaitForSeconds(sec);
            // StartDelayText.text="1";
            // yield return new WaitForSeconds(3);
            // StartDelayText.text="";
            //  StartDelayText.gameObject.SetActive(false);
            // GameStopped=false;
            yield return new WaitForSeconds(3f);
        
            // path.CheckAllBalls();

            
        }
    public void SummonPuf(Transform ball){
       Destroy( Instantiate(Puf,ball.position,Quaternion.identity,transform),1);
    }
}
