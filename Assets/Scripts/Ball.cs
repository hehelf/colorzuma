﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

   public int color;
   public bool Current = false;
   GameManager GM;
      ImageConrtol IMGC;
   	private MovementPath moveBallsScript;
    private MovementPath path;
	private bool onceFlag;


   
       void Start()
    {
        GM = GetComponentInParent<GameManager>();
        UpdColor();
        Current = false;
		path = GM.GetComponentInChildren<MovementPath>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       if(Current){SetVelocity();}

        if (GM.UpdColorOn){
            UpdColor();
        }
       
    }

    void UpdColor(){
        Color ballColor = GM.GetComponentInChildren<ImageConrtol>().BallsColors[color];

        GetComponent<MeshRenderer>().material.color = ballColor;
 
    }

        void SetVelocity(){
            transform.position += new Vector3(0,0,GM.BallFlightSpeed*0.1f);
            if(transform.position.magnitude>GM.FlightLength){
                Destroy(gameObject);
                
                    }
        }

    void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "ActiveBalls" && Current && gameObject.tag == "NewBall")
		{
			
			Current = false;
            
			this.gameObject.tag = "ActiveBalls";
			this.gameObject.layer = LayerMask.NameToLayer("ActiveBalls");

			ContactPoint contact = other.contacts[0];
			Vector3 CollisionDir = contact.point - other.transform.position;

			int currentIdx = other.transform.GetSiblingIndex();

			float angle  = Vector3.Angle(CollisionDir, other.transform.forward);
			if ( angle > 90)
				path.AddNewBallAt(this.gameObject, currentIdx + 1, currentIdx,other.transform.parent);
			else
				path.AddNewBallAt(this.gameObject, currentIdx, currentIdx,other.transform.parent);

		}

        if (transform.parent.GetSiblingIndex() < other.transform.parent.GetSiblingIndex()&&other.gameObject.tag == "ActiveBalls"){
           Debug.Log(other.transform.parent.name+"  " +transform.parent.name);
            path.ConnectChains(this, other.gameObject.GetComponent<Ball>()); 
        }
	}

        
}
