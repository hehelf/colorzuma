﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class Cannon : MonoBehaviour
{
    [SerializeField]Transform[] slots;
    [SerializeField]Transform[] borders;
    public List<GameObject> Balls = new List<GameObject>();

    [SerializeField]BGCurve cycle;
    [SerializeField]float ShootingDelay;
    [SerializeField]Animator GunAnim;
    
    private bool prevGameStopped;
    public float duration ;

    bool MouseDown = false;
   GameManager GM;
    Quaternion ballRotate = Quaternion.Euler(new Vector3(90,0,90));
    List<float> bordPosX = new List<float>();
    bool blow;
    bool ShootAllow;
      ImageConrtol IMGC;
      MovementPath path;    
    
    public bool switchBall;

    void Start()
    {
        GM = GetComponentInParent<GameManager>();
        

        bordPosX.Add(-4.83f*GameScaler.factor);
        bordPosX.Add(4.83f*GameScaler.factor);


        switchBall=false;
        blow = false;
        ShootAllow = true;
    }

    // Update is called once per frame
    void Update()
    {
        // UpdBallsInCannon();
                if(switchBall){
            if(Balls[0]!=null)FollowBall(Balls[0].transform,1);
            if(Balls[1]!=null)FollowBall(Balls[1].transform,0);
        }

        if(blow)
            if(Balls[1].GetComponent<Animation>().isPlaying){
            
        }else{
            if(blow)blow=false;
            Balls[1].GetComponent<Animation>().enabled=false;
        }

                
   if(GameManager.GameStopped) return;
            Vector3 cursor = Input.mousePosition;
            cursor = Camera.main.ScreenToWorldPoint(cursor);
           
            if(Input.GetMouseButton(0)){
                //  Debug.Log(bordPosX[0]+"   "+bordPosX[1]+"   "+cursor);
                float xPos = cursor.x;
                // if (cursor.x<bordPosX[0])xPos = bordPosX[0];
                // if (cursor.x>bordPosX[1])xPos = bordPosX[1];
                
                transform.DOMove(new Vector3( xPos,transform.position.y,transform.position.z),duration);
            }

           if(Input.GetMouseButtonUp(0)&&ShootAllow&&!blow){
            Shoot();
             } 
    }
    public void UpdBallsInCannon(){
    IMGC = GM.GetComponentInChildren<ImageConrtol>();
    path = GM.GetComponentInChildren<MovementPath>();
    if(Balls.Count<1){        
        Balls.Add(SpawnBall(slots[0].position));
        Balls.Add(SpawnBall(slots[1].position));
    }
        if(Balls[0]!=null)Destroy(Balls[0]);
        if(Balls[1]!=null)Destroy(Balls[1]);
        Balls[0] = (SpawnBall(slots[0].position));
        Balls[1]= (SpawnBall(slots[1].position));
    }


        void Shoot(){
             if(GameManager.GameStopped||MovementPath.final) return;
                // Balls[0].transform.position = ShootStartPos.position;
                Balls[0].GetComponent<Ball>().Current = true;
                Balls[0].transform.parent = GetComponentInParent<GameManager>().transform;
                // Balls[0].tag = "NewBall";
            Balls[0]=Balls[1];
            GunAnim.SetTrigger("shot");
            switchBall = true;
            StartCoroutine(ShootCD(ShootingDelay));
            // Balls[0].transform.position = slots[0].position;
            Balls[1]=null;

        }


        
    GameObject SpawnBall(Vector3 pos){
        GameObject ball = Instantiate(GM.BallPrefab,pos,Quaternion.identity,transform);
        ball.GetComponent<Ball>().color = RandomBall(IMGC.BallsForFullFill);
        ball.tag = "NewBall";
        return ball;
    }

    
    public void SetBallsInCannon(List<int> vals){
        // Balls[0].GetComponent<Ball>().value = vals[0];
        // Balls[1].GetComponent<Ball>().value = vals[1];
    }


    void FollowBall(Transform ball,int i){
        ball.position = slots[i].position;
    }
    
    public void SpawnNewBall(){
        Balls[1]=SpawnBall(slots[0].position);
        Balls[1].GetComponent<Animation>().enabled = true;
        blow = true;

    }
    
    IEnumerator ShootCD(float sec){
        ShootAllow = false;
        float tim = Time.time;
        yield return new WaitForSeconds(sec);
        // Debug.Log(tim-Time.time);
         ShootAllow = true;
    }

     int RandomBall(float[] fullFill){
         int value = 0;
        float rnd =  Random.Range(0.0f, 1.0f);
        float sum=0;
        int length = fullFill.Length;
        foreach (var el in fullFill)
        {
            sum+=el;
        }
        float[] prob= new float[length+1];
        for (int i = 0; i < length; i++)
        {
            prob[i+1]=fullFill[i]/sum+prob[i];
            
        }
        // Debug.Log(fullFill[0]/sum+"   "+prob[0]); 
        for (int i = 0; i < length; i++)
        {
            if(rnd>prob[i]&&rnd<=prob[i+1]){ 
                value  = i; 
                break;
                }
        }
        Debug.Log(value); 
                  //проверка на заполнность
        
            while(!IsNormalValue(value)){
                value = Random.Range(0,length);
                if(GameManager.GameStopped)break;
            }         
        
        return value;
    }

    bool IsNormalValue(int value){
        if(!IMGC.ColorComplete[value]) return true;
            foreach (var ball in path.GetComponentsInChildren<Ball>())
            {
                if(ball.color==value)return true;
            }
        return false;
    }
}
