﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ScoreManager : MonoBehaviour
{
    public TextMeshPro lvlText;
    public TextMeshPro NextlvlText;

    public Text scoreText;
    public Text highscoreText;
    public SpriteRenderer progress;

    public static int plvl;
    public static int exp;
    
    public static int score;
    
    public static int highscore;
    
    [SerializeField]GameObject lvlup;
    LevelTable lvlTable;
    BallTable ballTable;
    GameManager GM;

    void Start()
    {
        GM = GetComponent<GameManager>();
        LoadScore();
        LoadScoreConf();
        UpdText();
        SetStartProgress();

        
    }

    void Update(){
        UpdProgressBar();
    }

    public void UpdText(){
        lvlText.text =""+(plvl+1);
        NextlvlText.text =""+(plvl+2);
        scoreText.text = ""+score;
        highscoreText.text = ""+highscore;
    }
    // public void incScore(int ballValue){
    //     int minBall = GM.powOfWin+GM.level - GM.numberOfDifferentBalls;
    //     if(minBall <1) minBall = 1;
    //     int incVal = ballValue-minBall+1;
    //     int sc = (int)Mathf.Pow(2,incVal);
    //     exp += GetExp(incVal);
    //     score+= sc;
    //     HighscoreCheck();
    //     LvlUpCheck();
    //     UpdText();
    //     // UpdProgressBar();
    // }

    void HighscoreCheck(){
        if (score>highscore){
            highscore=score;
        }
    }
    public void Fail(){
        score = 0;
        
        // UpdText();
    }
    public void SaveScore(){
         GameData.ScoreData dat = new GameData.ScoreData(plvl,exp,score,highscore);
        string strData = JsonUtility.ToJson(dat);
        PlayerPrefs.SetString("ScoreData",strData);
        PlayerPrefs.Save();
    }
    void LoadScore(){
                if(PlayerPrefs.HasKey("ScoreData")){
            string strData = PlayerPrefs.GetString("ScoreData");
            GameData.ScoreData dat = JsonUtility.FromJson<GameData.ScoreData>(strData);
             plvl=dat._lvl;
             exp = dat._exp;
             score = dat._score;
             highscore = dat._highscore;
        }
    }

    void LoadScoreConf(){
         TextAsset JsonData=(TextAsset)Resources.Load("Config/levelTable"); 
            string json=JsonData.text;
            lvlTable = JsonUtility.FromJson<LevelTable>(json);
             JsonData=(TextAsset)Resources.Load("Config/ballTable"); 
             json=JsonData.text;
            ballTable = JsonUtility.FromJson<BallTable>(json);
    }
    int GetExp(int ball){
        return ballTable.ballTable[ball-1].exp;
    }
    int GetLvlUpExp(){
        int i = plvl;
        if(plvl>=lvlTable.levelTable.Length) i= lvlTable.levelTable.Length-1;
        return lvlTable.levelTable[i].exp;

    }

    void LvlUpCheck(){

        int needExp = GetLvlUpExp();
        if(exp>=needExp) {
            plvl++;
            exp-=needExp;
            StartCoroutine(LevelUp());
        }
    }

    void UpdProgressBar(){
        float prog = (float)exp/GetLvlUpExp()*1.0f;
        float fullValue = 7.06f;
        float value = fullValue*prog;
        float fill = progress.size.x;
        float newFill =Mathf.Lerp(fill,value,Time.fixedDeltaTime*10);
        progress.size = new Vector2(newFill,progress.size.y);

    }
    void SetStartProgress(){
        float prog = (float)exp/GetLvlUpExp()*1.0f;
        float fullValue = 7.06f;
        float value = fullValue*prog;
        progress.size = new Vector2(value,progress.size.y);
    }


    void Log(){
        string str ="";
        foreach (var item in lvlTable.levelTable)
        {
            str+=item.exp + " ; ";
        }
                string str1 ="";
        foreach (var item in ballTable.ballTable)
        {
            str1+=item.exp + " ; ";
        }
        Debug.Log(str);
        Debug.Log(str1);
    }
    public void RevertScore(){
        plvl=0  ;
        exp=0;
        score=0;
        highscore=0;
    }

    IEnumerator LevelUp(){
        lvlup.SetActive(true);
        yield return new WaitForSeconds(8);
        lvlup.SetActive(false);
    }
}
