﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;
using UnityEngine.UI;
using System.Linq;

public class MovementPath : MonoBehaviour
{
    public List<Transform> ballList;
   [SerializeField]float speed;
   [SerializeField]float ballRadius = 1.5f;
    private int addBallIndex;
	public Ease easeType = Ease.Linear;

    public float mergeSpeedFactor;
    public float StartSpeedFactor;
    public List<OneChain> chains = new List<OneChain>();
    public class OneChain {
        public Transform head(){
            return chainObj.transform.GetChild(0).transform;
        }
       public Transform end(){
            return chainObj.transform.GetChild(length()-1).transform;
        }
        public float headDist;
        public bool IsMoving;
        public GameObject chainObj;
        public OneChain(List<Transform> balls,float dist, Transform chainParent){
            headDist = dist;
            chainObj = new GameObject();
            chainObj.transform.parent = chainParent;
            foreach (var ball in balls)
            {
                ball.parent= chainObj.transform;
            }

            chainObj.transform.name = "chn"+chainObj.transform.GetSiblingIndex();
        }
        public int length(){
            return chainObj.transform.childCount;
        }
        public Transform GetBall(int i){

                return chainObj.transform.GetChild(i).transform;
        }
        public List<Transform> GetBalls(int from, int to){
            List<Transform> childs = new List<Transform>();
             for (int i = from; i <= to; i++)
             {
                 childs.Add(GetBall(i));
             }
            return childs;
        }

        public List<Transform> GetAllBalls(){
            List<Transform> childs = new List<Transform>();
             for (int i = 0; i < length(); i++)
             {
                 childs.Add(GetBall(i));
             }
            return childs;
        }
        public int GetIndex(){
            return chainObj.transform.GetSiblingIndex();
        }
    }

    [SerializeField]int chainGOBackIndex = -1;   
    [SerializeField]Text txt;
         float pathLenght;
    GameManager GM;
    bool IsOnStart;

    public static bool final;
    
   ImageConrtol IMGC;
    void Start(){
		addBallIndex = -1;
       GM = GetComponentInParent<GameManager>();
       IMGC = GM.IMGC;
       pathLenght = GetComponent<BGCcMath>().GetDistance();
    //    GM.cannon.UpdBallsInCannon();
       ballRadius*=GameScaler.factor;
    //    chains.Add( new OneChain(ballList,0,transform));
    }

    void Update(){
        if(GameManager.GameStopped) return;
        if (speed != GM.BallsSpeed*0.1f) speed = GM.BallsSpeed*0.1f;
    // ChainLog();
   AddBallsToChain();
   if(chains.Count==0)return;

        if(chains[chains.Count-1].headDist >=pathLenght){
                    GM.final(false);
                    }
    
                // на старте, если не выехала на 10 шаров или на всю длину цепочки, если она меньше 10 шаров
            IsOnStart = chains[0].headDist<=(chains[0].length()<10?chains[0].length():10)*ballRadius&&chains.Count==1;
		if (chainGOBackIndex ==-1){
			MoveAllBallsAlongPath();
            }

        if(chainGOBackIndex !=-1){
            if(chainGOBackIndex ==0)
                if(chains.Count>1){
                    chainGOBackIndex=1;
                    Debug.Log("count>1");
                }
                else{
                  chainGOBackIndex=-1;
                  Debug.Log("stop go back");
                }
            if(chainGOBackIndex>chains.Count-1)chainGOBackIndex=chains.Count-1;
            ChainsReverse();
        }
    }
    void AddBallsToChain(){
        List<Transform> balls = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).tag== "ActiveBalls") balls.Add(transform.GetChild(i));
        }
        if (balls.Count>0) {
                OneChain chn = new OneChain(balls,0,transform);
                chn.IsMoving = true;
                chains.Add( chn);
                SortChains();
        }
        
    }
void ChainLog()
{        string s = "";
            foreach (var c in chains)
            {
                s+=c.chainObj.name+"  "+c.headDist+"\n";
            }
            // txt.text = s;
            Debug.Log(s);
}
    public OneChain GetLastChain(){
        OneChain last = chains[0];
        // float dist = GetComponent<BGCcMath>().GetDistance();
        // foreach (var chn in chains)
        // {
        // if(chn.IsMoving&&chn.headDist<dist) last = chn;
        // }
        return last;
    }
private void MoveAllBallsAlongPath()
	{
        foreach (var chn in chains)
        {
            if(chn.IsMoving){
                Vector3 tangent;
                float spd = IsOnStart?speed*StartSpeedFactor:speed;
                chn.headDist += spd * Time.deltaTime;
            
                for (int i = 0; i < chn.length(); i++)
                {
                    float currentBallDist = chn.headDist - i * ballRadius;
                    Vector3 pos = GetComponent<BGCcMath>().CalcPositionAndTangentByDistance(currentBallDist , out tangent);
                    // if (i == addBallIndex && addBallIndex != -1)
                    //     chn.GetBall(i).DOMove(pos, 0.5f).SetEase(easeType);
                    // else
                        chn.GetBall(i).DOMove(pos, 0.25f);

                    chn.GetBall(i).rotation = Quaternion.LookRotation(tangent);
                }

            }
        }
		
	}
	public void AddNewBallAt(GameObject go, int index, int touchedBallIdx, Transform chain)
	{
		addBallIndex = index;
        OneChain thischn =  chains[chain.GetSiblingIndex()];

		go.transform.parent = chain;
		go.transform.SetSiblingIndex(index);

        if (thischn.IsMoving)
                thischn.headDist += 0.5f*ballRadius;
            else AddBallToStoppedChain(thischn);

        StartCoroutine(MatchCheckDelay(0.5f,go.transform));
	}

    void AddBallToStoppedChain(OneChain chn){
        Vector3 tangent;
       chn.headDist += 0.5f*ballRadius;
               for (int i = 0; i < chn.length(); i++)
        {
            float currentBallDist = chn.headDist - i * ballRadius;
			Vector3 pos = GetComponent<BGCcMath>().CalcPositionAndTangentByDistance(currentBallDist , out tangent);
			// if (i == addBallIndex && addBallIndex != -1)
			// 	chn.GetBall(i).DOMove(pos, 0.5f).SetEase(easeType);
			// else
				chn.GetBall(i).DOMove(pos, 0.25f);

            chn.GetBall(i).rotation = Quaternion.LookRotation(tangent);
        }

    }

    void MatchCheck(Transform ball){
        int i = ball.GetSiblingIndex();
        OneChain thischn = chains[ball.parent.GetSiblingIndex()];

        int thiscolor = thischn.GetBall(i).GetComponent<Ball>().color;
        
        int SameBefore = 0;
        int SameAfter = 0;
        List<int> sameColors = new List<int>();

        sameColors.Add(i);
     

        if (i<thischn.length())

             while((i+1+SameBefore<thischn.length())&&thiscolor == thischn.GetBall(i+1+SameBefore).GetComponent<Ball>().color){
                sameColors.Add(i+1+SameBefore);
                SameBefore++;
            }
        if (i>=0) 
            while((i-1-SameAfter>=0)&&thiscolor ==  thischn.GetBall(i-1-SameAfter).GetComponent<Ball>().color){
                sameColors.Add(i-1-SameAfter);
                    SameAfter++;
            }
        sameColors.Sort();

        //если матчитася больше 3х
        if (sameColors.Count>=3){
            int BeforeSplitLenght = thischn.length();

            List<Transform> MatchedBalls = thischn.GetBalls(sameColors[0],sameColors[sameColors.Count-1]);
            OneChain ChnGoBack = thischn;   
            if(!sameColors.Contains(0)&&!sameColors.Contains(thischn.length()-1)){
                      ChnGoBack = new OneChain(thischn.GetBalls(0,sameColors[0]-1),thischn.headDist,transform);
                    chains.Add(ChnGoBack);
                    Debug.Log("newCHN");
                    
                }

            if (!sameColors.Contains(BeforeSplitLenght-1))
            {
            Debug.Log("не содержит конец цепочки");
            
            thischn.headDist -= (sameColors.Count+sameColors[0]) * ballRadius;
            } else{
                if(!sameColors.Contains(0))
                {   
                       Debug.Log("не содержит начало цепочки, но содержит конец");
                    // thischn.headDist -= ballRadius;
                    }else{//содержин начало и конец цепочки
                                Debug.Log(" содержит конец цепочки и начало");
                            chains.Remove(thischn);
                            Destroy(thischn.chainObj.gameObject);
                    }
            }
            Debug.Log("убрать  "+ sameColors.Count+" шаров");
             RemoveMatched(MatchedBalls);
            IMGC.IncPart((int)thiscolor,sameColors.Count);  
            if(sameColors.Count >=5){
                chainGOBackIndex = chains.IndexOf(ChnGoBack);
            }
            
        }
       
    }
    
    void ChainsReverse(){
        if(chainGOBackIndex<1)return;
        for (int i = chainGOBackIndex; i < chains.Count(); i++)
        {
            ChainGoBack(chains[i]);
        }
    }

    void ChainGoBack(OneChain chn){
		Vector3 tangent;
		chn.headDist -= speed * mergeSpeedFactor * Time.deltaTime;

        for (int i = 0; i < chn.length(); i++)
        {
            float currentBallDist = chn.headDist - i * ballRadius;
			Vector3 pos = GetComponent<BGCcMath>().CalcPositionAndTangentByDistance(currentBallDist , out tangent);
			// if (i == addBallIndex && addBallIndex != -1)
			// 	chn.GetBall(i).DOMove(pos, 0.5f).SetEase(easeType);
			// else
				chn.GetBall(i).DOMove(pos, 0.25f);

            chn.GetBall(i).rotation = Quaternion.LookRotation(tangent);
        }
    }

        IEnumerator MatchCheckDelay(float sec, Transform ball){
            yield return new WaitForSeconds(sec);
            MatchCheck(ball);
        }
        void RemoveMatched(List<Transform> balls){

            
            foreach (var ball in balls)
            {
                if(ball.gameObject != null){
                GM.SummonPuf(ball);
                ball.DOKill();
                ball.gameObject.tag = "Matched";
                Destroy(ball.gameObject);   
                }      
            }
            SortChains();
        }

        public void ConnectChains(Ball ball1, Ball ball2){
            int i1 = ball1.transform.parent.GetSiblingIndex();
            int i2 = ball2.transform.parent.GetSiblingIndex();
                  OneChain first = chains[Mathf.Min(i1,i2)];
                  OneChain second = chains[Mathf.Max(i1,i2)];
                // if(ball1.transform==first.end()&&ball2.transform == second.head()){
                //    second = chains[ball1.transform.parent.GetSiblingIndex()];
                //     first= chains[ball2.transform.parent.GetSiblingIndex()];

                // }

            // if (ball1.transform != first.head()) return;
            //  if (ball2.transform != second.end()) return;
             Debug.Log("connect "+first.chainObj.name+" & "+second.chainObj.name);
                    int secondLenghtBeforeMerge = second.length();
                    first.headDist = second.headDist;

                    for (int i = 0; i < secondLenghtBeforeMerge; i++)
                    {
                        Transform ball = second.end();
                        ball.parent = first.chainObj.transform;
                        ball.SetSiblingIndex(0);
                    }
                            chains.Remove(second);
                            Destroy(second.chainObj.gameObject);
                            Debug.Log("connected");
                            SortChains();
                            if (chainGOBackIndex!=-1) {
                                if(first!=chains[0]&&chains.Count>1)chainGOBackIndex--;
                                   
                                if(ball1.color==ball2.color){StartCoroutine(MatchCheckDelay(0.1f,ball2.transform));}
                                                               
                                }

        }
    // void ppSortChains(){
    //     if(chains.Count<=0)return;
    //     SortedList<float,int> headList = new SortedList<float, int>();

    //     List<float> heads= new List<float>();
    //     foreach (var chn in chains)
    //     {
    //         heads.Add(chn.headDist);
    //     }
    //     if(heads.Distinct().Count()!=heads.Count)return;
    // //     List<float[]> headList = new List<float[]>();
    //     for(int i = 0; i<chains.Count;i++)
    //     {   
    //         headList.Add(heads[i],i);
    //         Debug.Log("add to headlist");
    //     }
        
    //     List<OneChain> ChnList = chains;
        
    //     string s = "total = "+headList.Count+": ";
    //     for (int i = 0; i < headList.Count; i++)
    //     {
    //         chains[headList.Values[i]].chainObj.transform.SetSiblingIndex( i);
    //         s+= headList.Keys[i]+"  "+headList.Values[i]+" ; ";
    //     }
        
    //     for (int i = 0; i < headList.Count; i++)
    //     {
    //         chains[i]= ChnList[headList.Values[i]];
    //     }

    //     Debug.Log(s);
    // }
    void SortChains(){
        if(chains.Count<=0)return;
        chains.Sort((x, y) => x.headDist.CompareTo(y.headDist));
        string s = "total = "+chains.Count+": ";
        for (int i = 0; i < chains.Count; i++)
        {
            chains[i].chainObj.transform.SetSiblingIndex( i);
            s+= chains[i].headDist+"  "+i+" ; ";
        }
        Debug.Log(s);
    }
}
