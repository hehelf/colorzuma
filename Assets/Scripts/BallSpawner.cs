﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Components;
using BansheeGz.BGSpline.Curve;

public class BallSpawner : MonoBehaviour
{
   GameManager GM;
   [SerializeField]float WaveTimeOut;
   public List <int> Waves;
   [SerializeField]List <Vector3> ColorDistribution;
   public int CurrentWave=-1;
   MovementPath path;
   ImageConrtol IMGC;
    Vector3 spawnPos;
    bool waveEnd;
    float pausedTime=-1;
//    [SerializeField]int ballNumber;
   float prevWaveEnd=0;
    void Start()
    {   
        waveEnd = true;
        GM = GetComponentInParent<GameManager>();
        path = GetComponent<MovementPath>(); 
        spawnPos = GetComponent<BGCurve>().Points[0].PositionWorld;
        IMGC = GM.GetComponentInChildren<ImageConrtol>();

        // SpawnWave(0);
    }

    void Update(){

        if(GameManager.GameStopped){
            if(pausedTime==-1)WavePause();
            return;
        }
         if(pausedTime!=-1)WaveUnpause();
              if (path.chains.Count!=0)
                //  Debug.Log(path.GetLastChain().chainObj.name);

     if( WaveEndCheck()&&!waveEnd){
            waveEnd=true;
            prevWaveEnd=Time.time;
            Debug.Log("Ended " + Time.time );
        }

        // if(CurrentWave==-1||Time.time-prevWaveEnd > WaveTimeOut&&CurrentWave<Waves.Count-1&&waveEnd){
        if(CurrentWave==-1||Time.time-prevWaveEnd > WaveTimeOut&&waveEnd){
            Debug.Log("Started " + Time.time );
            waveEnd = false;
            CurrentWave++;
            SpawnWave(0);
            // prevWaveEnd = Time.time;
        }


    }

    bool WaveEndCheck(){
        if (path.chains.Count==0) return false;
     
        Vector3 c = path.GetLastChain().end().position-spawnPos;
        // Debug.Log(c.magnitude);
        return c.magnitude>=0.1f;
    }

    void SpawnWave(int i){
       
       
        for (int j = 0; j < Waves[i]; j++)
                {
                GameObject ball =  SpawnBall(spawnPos);
                    ball.tag = "ActiveBalls";
                }
    }

    GameObject SpawnBall(Vector3 pos){
        GameObject ball = Instantiate(GM.BallPrefab,pos,Quaternion.identity,transform);
        ball.GetComponent<Ball>().color = RandomBall(IMGC.BallsForFullFill);
        
        return ball;
    }
     int RandomBall(float[] fullFill){
         int value = 0;
        float rnd =  Random.Range(0.0f, 1.0f);
        float sum=0;
        int length = fullFill.Length;
        foreach (var el in fullFill)
        {
            sum+=el;
        }
        float[] prob= new float[length+1];
        for (int i = 0; i < length; i++)
        {
            prob[i+1]=fullFill[i]/sum+prob[i];
            
        }
        for (int i = 0; i < length; i++)
        {
            if(rnd>prob[i]&&rnd<=prob[i+1]){ 
                value  = i; 
                break;
                }
        }
            //проверка на заполнность
            while(IMGC.ColorComplete[value]){
                value = Random.Range(0,length);
            }            
        
        return value;
    }

    public void WavePause(){
            pausedTime = Time.time - prevWaveEnd;
    }
    
    public void WaveUnpause(){
            prevWaveEnd = Time.time - pausedTime;
            pausedTime=-1;
    }
}
