﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimEventsManager : MonoBehaviour
{
    int timerCount= 0;
    Cannon can;
    Text timerText;
    void Start(){
       Cannon can = GetComponentInParent<Cannon>();
    }
    public void StopFollow(){
       Cannon can = GetComponentInParent<Cannon>();
        can.switchBall = false;
    }

        public void SpawnNewBall(){
             Cannon can = GetComponentInParent<Cannon>();
            can.SpawnNewBall();
    }
    public void TimerStart(){
        timerText = GetComponentInParent<Text>();
        timerCount = 3;
        timerText.text = ""+timerCount;     
    }

    public void IncTimer(){
        timerCount--;
        if(timerCount>0)
            timerText.text = ""+timerCount;
        else {
            timerText.text = "Go";
             GameManager.GameStopped=false;
                 }
    }
    public void StopTimer(){
        gameObject.SetActive(false);
    }
    public void FinalMerge(){
        // GetComponentInParent<GameManager>().final(true);
    }
}
