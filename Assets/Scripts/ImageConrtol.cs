﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageConrtol : MonoBehaviour
{
    public AppearBehaviour[] Parts;
    public float[] BallsForFullFill;
    public float[] PartsFill;
    public Color[] BallsColors;
    public bool[] ColorComplete;

    void Awake(){
        PartsFill = new float[Parts.Length];
        ColorComplete=new bool[Parts.Length];
    }



    public void IncPart(int i){
         float step = 1/BallsForFullFill[i];
        Parts[i].DoAppear(step);
    }
    public void IncPart(int i,int n){
        float step = 1/BallsForFullFill[i];
        float fill = step*n;
        PartsFill[i]+=fill;
        if(PartsFill[i]>=1) ColorComplete[i]=true;
       Parts[i].SetAppear(PartsFill[i]);
    }

    public void SetPartFill(int i,float value){
        Parts[i].SetAppear(value);
        
    }


}
